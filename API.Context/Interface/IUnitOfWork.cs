﻿using System;
using System.Threading.Tasks;

namespace API.Context.Interface
{
    public interface IUnitOfWork<TContext> : IDisposable
        where TContext : IContext
    {
        int Save();
        Task<int> SaveAsync();
        TContext Context { get; }

    }
}
