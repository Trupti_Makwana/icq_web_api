using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace API.Context.Interface
{
    public interface IContext : IDisposable
    {
       int SaveChanges();
       void SetModified(object entity);
       void SetAdd(object entity);
       EntityEntry Entry(object entity);
       DbSet<TEntity> Set<TEntity>() where TEntity : class;
       Task<int> SaveChangesAsync();
  }
}
