using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace API.Context.Interface
{
    public interface IEntityRepository<TEntity> : IDisposable
    {
        IQueryable<TEntity> All { get; }
        IQueryable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties);
        IEnumerable<TEntity> FindByInclude(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);
        IEnumerable<TEntity> FindOrderByInclude(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> orderBy = null, params Expression<Func<TEntity, object>>[] includeProperties);
        IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        TEntity Find(Guid id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);

    }
}
