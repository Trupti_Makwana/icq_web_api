using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using API.Context.Interface;
using Microsoft.EntityFrameworkCore;

namespace API.Context.Base
{
   public abstract class GenericRepository<TContext, TEntity> : IEntityRepository<TEntity> where TEntity : class where TContext : IContext, IDisposable
   {
        protected readonly TContext Context;
        private readonly DbSet<TEntity> DbSet;
      protected GenericRepository(IUnitOfWork<TContext> uow)
      {
        this.Context = uow.Context;
        DbSet = Context.Set<TEntity>();
      }
      public IQueryable<TEntity> All => Context.Set<TEntity>();
      public virtual void Add(TEntity entity)
      {
       Context.SetAdd(entity);
      }
      public IQueryable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
      {
        return GetAllIncluding(includeProperties);
      }
      public IEnumerable<TEntity> FindByInclude(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
      {
        var query = GetAllIncluding(includeProperties);
        IEnumerable<TEntity> results = query.Where(predicate).ToList();
        return results;
      }
      public IEnumerable<TEntity> FindOrderByInclude(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> orderBy = null, params Expression<Func<TEntity, object>>[] includeProperties)
      {
         var query = GetAllIncluding(includeProperties);
         query = query.Where(predicate);
         if (orderBy != null)
         query = query.OrderByDescending(orderBy);
         IEnumerable<TEntity> results = query.ToList();
         return results;
      }
      public IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
      {
         IQueryable<TEntity> queryable = DbSet.AsNoTracking();
         IEnumerable<TEntity> results = queryable.Where(predicate).ToList();
         return results;
      }
      private IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
      {
        IQueryable<TEntity> queryable = DbSet.AsNoTracking();
        return includeProperties.Aggregate(queryable, (current, includeProperty) => current.Include(includeProperty));
      }
      public TEntity Find(Guid id)
      {
        return Context.Set<TEntity>().Find(id);
      }
      public virtual void Update(TEntity entity)
      {
        Context.SetModified(entity);
      }
      public virtual void Delete(int id)
      {
        var entity = Context.Set<TEntity>().Find(id);
        if (entity != null)
        {
           Context.Set<TEntity>().Remove(entity);
        }
      }
      public void Dispose()
      {
         Context.Dispose();
      }
   }
} 
