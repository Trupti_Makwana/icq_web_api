using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace API.Context.Base
{
   public class BaseContext<TContext>: DbContext where TContext : DbContext
   {
      protected BaseContext(DbContextOptions<TContext> options): base(options)
      {

      }
      public override int SaveChanges()
      {
        var entries = ChangeTracker.Entries().Where(e => e.Entity is BaseEntity && (e.State == EntityState.Added || e.State == EntityState.Modified));
        foreach (var entityEntry in entries)
        {
           if (entityEntry.State == EntityState.Modified)
           {
             ((BaseEntity)entityEntry.Entity).ModifiedOn = DateTime.UtcNow;
             ((BaseEntity)entityEntry.Entity).ModifiedBy = GlobalModel.UserId.ToString();
           }
           if (entityEntry.State == EntityState.Added)
           {
             ((BaseEntity)entityEntry.Entity).CreatedOn = DateTime.UtcNow;
             ((BaseEntity)entityEntry.Entity).CreatedBy = GlobalModel.UserId.ToString();
           }
        }
         return base.SaveChanges();
      }
     public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
     {
        return base.SaveChangesAsync(cancellationToken);
     }
     public void SetModified(object entity)
     {
        Update(entity);
     }
     public void SetAdd(object entity)
     {
        Add(entity);
     }

   }
}
