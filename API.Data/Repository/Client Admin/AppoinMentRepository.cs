using System;
using System.Linq;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
namespace API.Data.Repository.Client_Admin
{

  public class AppoinMentRepository : GenericRepository<IDataContext, AppoinMentModel>, IAppoinMent
  {
    public AppoinMentRepository(IUnitOfWork<IDataContext> uow) : base(uow)
    {

    }
 
    //public async Task<string> Handle(ConfirmAppoinMent request)
    //{
    //  try
    //  {
    //    var entity = await Context.AppoinMent.FindAsync(request.AppointmentId);

    //    if (entity == null)
    //    {
    //      return "Error while updating Category!";
    //    }
    //    else
    //    {
    //      entity.IsAccept = request.IsAccept;
    //      await Context.SaveChangesAsync();
    //    }
    //    sendMail(request);
    //    return "Appointment set Sucessfully.....";

    //  }
    //  catch (Exception ex)
    //  {
    //    throw ex;
    //  }
    //}

    //public async void sendMail(ConfirmAppoinMent request)
    //{
    //  string body = string.Empty;
    //  if (request.IsAccept == true)
    //  {
    //    String FullName = String.Format(request.FullName);
    //    DateTime AppointmentDate = DateTime.Parse(request.AppointmentDate.ToString());
    //    body = "<b>Dear Mr/Mrs/Ms.</b><br />Thank you,your Appointment has been Successfully Scheduled on Date is " + AppointmentDate.ToString("MMMM dd, yyyy") + " and Time on " + request.AppointmentTime + "<br />" + " <b> Service Provider : <b /> " + request.FullName;
    //  }
    //  else
    //  {

    //    DateTime AppointmentDate = DateTime.Parse(request.AppointmentDate.ToString());
    //    body = " Unfortunately, a Conflict has arisen in my Scheduled, and i must Cancle Your Appointment... <br />" + AppointmentDate.ToString("MMMM dd, yyyy") + " and Time on " + request.AppointmentTime;
    //  }

    //  MailMessage mail = new MailMessage();
    //  SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
    //  mail.From = new MailAddress("test.epistic@gmail.com");
    //  mail.To.Add(request.Email);
    //  mail.Subject = "Appointment Confirm";
    //  mail.Body = body;
    //  mail.IsBodyHtml = true;
    //  SmtpServer.Port = 587;
    //  SmtpServer.Credentials = new System.Net.NetworkCredential("test.epistic@gmail.com", "epistic1011");
    //  SmtpServer.EnableSsl = true;
    //  ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
    //  SmtpServer.Send(mail);
    //}


    public GetAppoinMentById GetAppoinMentById(Guid id)
    {
      GetAppoinMentById objAppoinMent = Context.AppoinMent.Where(g => g.AppointmentId == id).Select(c => new GetAppoinMentById
      {
        AppointmentId = c.AppointmentId,
        AppointmentDate = c.AppointmentDate,
        AppointmentTime = c.AppointmentTime,
        BookingServiceFeedbackId = c.BookingServiceFeedbackId,
        CustomerId = c.CustomerId,
        ConsultantId = c.ConsultantId,
        Consultant = null,
        Customer = null,
        BookingServiceFeedback = null
      }).FirstOrDefault();

      objAppoinMent.Consultant = Context.Consultant.Where(x => x.ConsultantId == objAppoinMent.ConsultantId).Select(x => new GetConsultantById
      {
        ConsultantId = x.ConsultantId,
        FirstName = x.FirstName,
        LastName = x.LastName,
        Email = x.Email,
        Password = x.Password,
        PhoneNo = x.PhoneNo
      }).ToList();

      objAppoinMent.Customer = Context.Customer.Where(x => x.CustomerId == objAppoinMent.CustomerId).Select(x => new GetCustomerById
      {
        CustomerId = x.CustomerId,
        FirstName = x.FirstName,
        LastName = x.LastName,
        Email = x.Email,
        PhoneNo = x.PhoneNo
      }).ToList();

      objAppoinMent.BookingServiceFeedback = Context.BookingServiceFeedback.Where(x => x.ServiceFeedbackId == objAppoinMent.BookingServiceFeedbackId).Select(x => new GetBookingServiceFeedbackId
      {
        ServiceFeedbackId = x.ServiceFeedbackId,
        BookingServiceCode = x.BookingServiceCode,
        ServiceRating = x.ServiceRating,
        CustomerComment = x.CustomerComment
      }).ToList();
      return objAppoinMent;
    }

  
  }

}
