using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository.Client_Admin
{
     public class CustomerRepository : GenericRepository<IDataContext, CustomerDto>, ICustomer
     {

        public CustomerRepository(IUnitOfWork<IDataContext> uow) : base(uow)
        {

        }
        public GetCustomerById GetById(Guid id)
        {
            GetCustomerById objCustomer = Context.Customer.Where(g => g.CustomerId == id).Select(c => new GetCustomerById
            {
               CustomerId = c.CustomerId,
               FirstName = c.FirstName,
               LastName = c.LastName,
               Email = c.Email,
               PhoneNo = c.PhoneNo,
               ProfilePic = c.ProfilePic,
               IsActive = c.IsActive,
               CustomerAddress = null
            }).FirstOrDefault();

             objCustomer.CustomerAddress = Context.CustomerAddress.Where(x => x.CustomerId == objCustomer.CustomerId).Select(x => new GetCustomerAddressById
             {
                 ZipCode = x.ZipCode
             }).ToList();
             return objCustomer;
        }
         public List<GetAllCustomerList> GetAllCustomerList()
         {
           return Context.Customer.Include(a => a.CustomerAddress).Select(h => new GetAllCustomerList
           {
             CustomerId = h.CustomerId,
             FirstName = h.FirstName,
             LastName = h.LastName,
             Email = h.Email,
             PhoneNo = h.PhoneNo,
             ProfilePic = h.ProfilePic,
             CustomerAddress = h.CustomerAddress
           }).ToList();
      }
   }
}
