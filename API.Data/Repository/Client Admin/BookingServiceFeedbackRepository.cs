using System;
using System.Collections.Generic;
using System.Linq;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository.Client_Admin
{
  public class BookingServiceFeedbackRepository : GenericRepository<IDataContext, BookingServiceFeedbackModel>, IBookingServiceFeedback
  {

    public BookingServiceFeedbackRepository(IUnitOfWork<IDataContext> uow) : base(uow)
    {

    }

    public GetBookingServiceFeedbackId GetById(Guid id)
    {
      GetBookingServiceFeedbackId objConsultant = Context.BookingServiceFeedback.Where(g => g.ConsultantId == id).Select(c => new GetBookingServiceFeedbackId
      {
        ServiceFeedbackId = c.ServiceFeedbackId,
        BookingServiceCode = c.BookingServiceCode,
        ServiceRating = c.ServiceRating,
        CustomerComment = c.CustomerComment,
        ConsultantId = c.ConsultantId,
        CustomerId = c.CustomerId,
        Consultant = null,
        Customer = null
      }).FirstOrDefault();

      objConsultant.Consultant = Context.Consultant.Where(x => x.ConsultantId == objConsultant.ConsultantId).Select(x => new GetConsultantById
      {
        ConsultantId = x.ConsultantId,
        FirstName = x.FirstName,
        LastName = x.LastName,
        Email = x.Email,
        Password = x.Password,
        PhoneNo = x.PhoneNo
      }).ToList();

      objConsultant.Customer = Context.Customer.Where(x => x.CustomerId == objConsultant.CustomerId).Select(x => new GetCustomerById
      {
        CustomerId = x.CustomerId,
        FirstName = x.FirstName,
        LastName = x.LastName,
        Email = x.Email,
        PhoneNo = x.PhoneNo
      }).ToList();
      return objConsultant;
    }
    public List<GetAllBookingServiceFeedbackList> GetAllBookingServiceFeedbackList()
    {
      return Context.BookingServiceFeedback.Select(h => new GetAllBookingServiceFeedbackList
      {
        ServiceFeedbackId = h.ServiceFeedbackId,
        BookingServiceCode = h.BookingServiceCode,
        ServiceRating = h.ServiceRating,
        ConsultantId = h.ConsultantId,
        CustomerId = h.CustomerId
      }).ToList();
    }

  }
}
