using System.Collections.Generic;
using System.Linq;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;

namespace API.Data.Repository.Client_Admin
{
  public class CategoryRepository : GenericRepository<IDataContext, CategoryModel>, ICategory
  {
    public CategoryRepository(IUnitOfWork<IDataContext> uow) : base(uow)
    {

    }
    public List<GetAllCategoryList> GetAllCategoryList()
    {
      var vStateName = (from C in Context.Category
                        where (C.IsActive == true && C.IsDeleted == false)
                        orderby C.CategoryName
                        select new GetAllCategoryList
                        {
                          CategoryId = C.CategoryId,
                          CategoryName = C.CategoryName
                        });

      return vStateName.ToList();
      //return Context.Category.Select(h => new GetAllCategoryList
      //{
      //  CategoryId = h.CategoryId,
      //  CategoryName = h.CategoryName
      //}).ToList();
    }
  }
}
