using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;

namespace API.Data.Repository.Client_Admin
{
   public class TableOfTableRepository : GenericRepository<IDataContext, TableOfTablemodel>, ITableOfTable
   {
      public TableOfTableRepository(IUnitOfWork<IDataContext> uow) : base(uow)
      {

      }
   }
}
