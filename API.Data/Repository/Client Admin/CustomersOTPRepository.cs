using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface;
using API.Core.Model.Client_Admin;
using System;
using System.Collections.Generic;
using System.Linq;


namespace API.Data.Repository.Client_Admin
{
  public class CustomersOTPRepository : GenericRepository<IDataContext, CustomerOTP>, ICustomerOTP
  {
    public CustomersOTPRepository(IUnitOfWork<IDataContext> uow) : base(uow)
    {

    }
    // Customer GenerateOTP 
    public string GenerateOTP(AddCustomersOTP request, int iOTPLength, string[] saAllowedCharacters)
    {
      {

        try
        {

          if (!String.IsNullOrEmpty(request.PhoneNo))
          {
            var IsOtpExist = (from r in Context.Customer
                              join j in Context.CustomersOTP on r.CustomerId equals j.CustomerId
                              where r.PhoneNo == request.PhoneNo
                              select j).FirstOrDefault();

            if (IsOtpExist != null)
            {
              return IsOtpExist.OTP;
            }
            else
            {
              Random generator = new Random();
              int otpnumber = generator.Next(1000, 10000);
              var customermodel = new CustomerDto
              {
                //CustomerId = new Guid(),
                PhoneNo = request.PhoneNo,
                IsActive = true,

              };

              Context.Customer.Add(customermodel);
              Context.SaveChanges();
              var customermodeladdress = new CustomerAddressModel
              {
                //CustomerAddressId = new Guid(),
                CustomerId = customermodel.CustomerId
              };

              Context.CustomerAddress.Add(customermodeladdress);
              Context.SaveChanges();
              var customerotpmodel = new CustomerOTP
              {
                //CustomersOTPId = new Guid(),
                CustomerId = customermodel.CustomerId,
                OTP = otpnumber.ToString()
              };

              Context.CustomersOTP.Add(customerotpmodel);
              Context.SaveChanges();
              return otpnumber.ToString();
            }

          }
          else
          {
            return "0";
          }
        }
        catch (Exception ex)
        {
          throw ex;
        }

        string sOTP = String.Empty;

        string sTempChars = String.Empty;

        Random rand = new Random();

        for (int i = 0; i < iOTPLength; i++)
        {
          int p = rand.Next(0, saAllowedCharacters.Length);

          sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];

          sOTP += sTempChars;
        }
        return sOTP;
      }

    }
    // Customer VerifyOTP 

    public CustomerDto VerifyOTP(VerifyOTP request)
    {
      CustomerDto IsOTPValid = new CustomerDto();
      if (!String.IsNullOrEmpty(request.PhoneNo))
      {
        IsOTPValid = (from r in Context.Customer
                      join j in Context.CustomersOTP on r.CustomerId equals j.CustomerId
                      where r.PhoneNo == request.PhoneNo && j.OTP == request.OTP
                      select r).FirstOrDefault();

        if (IsOTPValid != null)
        {
          return IsOTPValid;
        }
        else
        {
          return IsOTPValid;
        }
      }
      return IsOTPValid;
    }

    // Customer ResendOTP
    public CustomerOTP ResendOTP(ResendOTP request)
    {
      Random generator = new Random();
      int OtpNumber = generator.Next(1000, 10000);

      var updateOTP = (from r in Context.Customer
                       join j in Context.CustomersOTP on r.CustomerId equals j.CustomerId
                       where r.PhoneNo == request.PhoneNo
                       select j).FirstOrDefault();
      if (updateOTP != null)
      {
        CustomerOTP entity = Context.CustomersOTP.Where(x => x.CustomersOTPId.ToString() == updateOTP.CustomersOTPId.ToString()).FirstOrDefault();
        entity.OTP = OtpNumber.ToString();
        return entity;
      }
      else
      {
        return null;
      }
    }
  }
}

