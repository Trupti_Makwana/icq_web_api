using System.Collections.Generic;
using System.Linq;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;

namespace API.Data.Repository.Client_Admin
{
    public class CityRepository : GenericRepository<IDataContext, CityModel>, ICity
    {
    public CityRepository(IUnitOfWork<IDataContext> uow) : base(uow)
    {

    }
    public List<GetAllCityList> GetCityList()
    {
      return Context.City.Select(h => new GetAllCityList
      {
        CityId = h.CityId,
        CityName = h.CityName
      }).ToList();
    }
  }
}
