using System;
using System.Collections.Generic;
using System.Linq;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository.Client_Admin
{
   public class ConsultantReposotry : GenericRepository<IDataContext, ConsultantDto>, IConsultant
   {
       public ConsultantReposotry(IUnitOfWork<IDataContext> uow) : base(uow)
       {
      
       }

       public GetConsultantById GetById(Guid id)
       {
          GetConsultantById objConsultant = Context.Consultant.Where(g => g.ConsultantId == id).Select(c => new GetConsultantById
          {
           ConsultantId = c.ConsultantId,
           FirstName = c.FirstName,
           LastName = c.LastName,
           Email = c.Email,
           PhoneNo = c.Password,
           ProfilePic = c.PhoneNo,
           AltPhoneNo = c.AltPhoneNo,
           Gender = c.Gender,
           DateofBirth = c.DateofBirth,
           IsApproved = c.IsApproved,
           IsAvailable = c.IsAvailable,
           ConsultantAddress = null
          }).FirstOrDefault();

          objConsultant.ConsultantAddress = Context.ConsultantAddress.Where(x => x.ConsultantId == objConsultant.ConsultantId).Select(x => new GetConsultantAddressById
          {
             ConsultantAddressId = x.ConsultantAddressId,
             ConsultantId = x.ConsultantId,
             Address = x.Address,
             ZipCode = x.ZipCode
          }).ToList();
          return objConsultant;
       }
    public List<GetAllConsultantList> GetallConsultantList()
    {
      return Context.Consultant.Include(a => a.ConsultantAddress).Select(h => new GetAllConsultantList
      {
        ConsultantId = h.ConsultantId,
        FirstName = h.FirstName,
        LastName = h.LastName,
        Email = h.Email,
        PhoneNo = h.PhoneNo,
        ConsultantAddress = h.ConsultantAddress
      }).ToList();
    }
    //public List<GetConsultantList> GetAllConsultantList()
    //{
    //  return Context.Consultant.Include(a => a.ConsultantAddress).Select(h => new GetConsultantList
    //  {
    //    ConsultantId = h.ConsultantId,
    //    FirstName = h.FirstName,
    //    LastName = h.LastName,
    //    Email = h.Email,
    //    PhoneNo = h.PhoneNo,
    //    ProfilePic = h.ProfilePic,
    //    ConsultantAddress = h.ConsultantAddress
    //  }).ToList();
    //}

  }
}
