﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using API.Context.Base;
using API.Context.Interface;
using API.Context.Pagination;
using API.Core;
using API.Core.Interface;
using API.Core.Model;
using API.Core.Model.EmailService;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using MimeKit;

namespace API.Data.Repository
{
    public class EmailService : GenericRepository<IDataContext, Message>, IEmailService
    {
        private readonly appsettings _appsettings;
        public EmailService(IUnitOfWork<IDataContext> uow,IOptions<appsettings> appsettings) : base(uow)
        {
            _appsettings = appsettings.Value;
        }

        public void SendEmail(Message message)
        {
            var emailMessage = CreateEmailMessage(message);
            Send(emailMessage);
        }
        private MimeMessage CreateEmailMessage(Message message)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_appsettings.EmailConfiguration.From));
            emailMessage.To.AddRange(message.To);
            emailMessage.Subject = message.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = message.Content };
            return emailMessage;
        }
        private void Send(MimeMessage mailMessage)
        {
            using (var client = new SmtpClient())
            {
                try
                {
                    client.Connect(_appsettings.EmailConfiguration.SmtpServer, _appsettings.EmailConfiguration.Port, true);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_appsettings.EmailConfiguration.Username, _appsettings.EmailConfiguration.Password);
                    client.Send(mailMessage);
                }
                catch
                {
                    //log an error message or throw an exception or both.
                    throw;
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();
                }
            }
        }

    }

}