﻿using System;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface;
using API.Core.Model.Common;

namespace API.Data.Repository
{
    public class CommonRepository : GenericRepository<IDataContext, Common>, ICommon
    {
        public CommonRepository(IUnitOfWork<IDataContext> uow) : base(uow)
        {

        }

        public string RandomGenerator(int iOTPLength, string[] saAllowedCharacters)
        {

            string sOTP = String.Empty;

            string sTempChars = String.Empty;

            Random random = new Random();

            for (int i = 0; i < iOTPLength; i++)
            {

                int p = random.Next(0, saAllowedCharacters.Length);

                sTempChars = saAllowedCharacters[random.Next(0, saAllowedCharacters.Length)];

                sOTP += sTempChars;

            }

            return sOTP;
        }
    }

}