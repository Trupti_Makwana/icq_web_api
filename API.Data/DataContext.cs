using API.Context.Base;
using API.Core;
using API.Core.Model;
using API.Core.Model.Client_Admin;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace API.Data
{
    public class DataContext : BaseContext<DataContext>, IDataContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        
        }
        public DbSet<ConsultantDto> Consultant { get; set; }
        public DbSet<CustomerDto> Customer { get; set; }
        public DbSet<ConsultantAddressModel> ConsultantAddress { get; set; }
        public DbSet<CustomerAddressModel> CustomerAddress { get; set; }
        public DbSet<CategoryModel> Category { get; set; }
        public DbSet<ContactUsModel> ContactUs { get; set; }
        public DbSet<CityModel> City { get; set; }
        public DbSet<BookingModel> Booking { get; set; }
        public DbSet<TableOfTablemodel> TableOfTable { get; set; }
        public DbSet<CustomerOTP> CustomersOTP { get; set; }
        public DbSet<ConsultantDocumentsModel> ConsultantDocuments { get; set; }
        public DbSet<AppoinMentModel> AppoinMent { get; set; }
        public  DbSet<BookingServiceFeedbackModel> BookingServiceFeedback { get; set; }

    public Task<int> SaveChangesAsync()
    {
      throw new System.NotImplementedException();
    }
  }
}
