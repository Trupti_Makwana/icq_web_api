using API.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using API.Context.Base;
using API.Context.Interface;
using API.Core;
using API.Core.Interface;
using API.Data.Repository;
using Microsoft.EntityFrameworkCore;
using API.Core.Interface.Client_Admin;
using API.Data.Repository.Client_Admin;

namespace API.Extension
{
  public static class APIExtension
  {
    public static void addAPIDomain(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddDbContext<DataContext>(o =>
      {
        var connectionString = configuration["ConnectionStrings:Icq_Web_ApiContext"];
        o.UseSqlServer(connectionString);
        o.EnableSensitiveDataLogging();
      });

      services.AddScoped<IDataContext, DataContext>();
      services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
      services.AddScoped<IEmailService, EmailService>();
      services.AddScoped<ICommon, CommonRepository>();
      services.AddScoped<IConsultant, ConsultantReposotry>();
      services.AddScoped<IConsultantAddress, ConsultantAddressRepositiry>();
      services.AddScoped<IConsultantDocuments, ConsultantDocumentRepositiry>();
      services.AddScoped<ICategory, CategoryRepository>();
      services.AddScoped<IContactUs, ContactUsRepository>();
      services.AddScoped<ICity, CityRepository>();
      services.AddScoped<ICustomer, CustomerRepository>();
      services.AddScoped<ICustomerAddress, CustomerAddressRepository>();
      services.AddScoped<IBookingServiceFeedback, BookingServiceFeedbackRepository>();
      services.AddScoped<IBooking, BookingRepository>();
      services.AddScoped<ITableOfTable, TableOfTableRepository>();
      services.AddScoped<ICustomerOTP, CustomersOTPRepository>();
      services.AddScoped<IAppoinMent, AppoinMentRepository>();
    }
  }
}
