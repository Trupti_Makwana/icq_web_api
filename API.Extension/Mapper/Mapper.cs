using API.Core.Model.Client_Admin;
using AutoMapper;

namespace API.Extension.Mapper
{

  public class AutoMapperProfile : Profile
  {
    public AutoMapperProfile()
    {
     
      // Cunsulatant  
      CreateMap<ConsultantDto, AddConsultant>().ReverseMap();
      CreateMap<ConsultantAddressModel, AddConsultantAddressModel>().ReverseMap();
      CreateMap<ConsultantDto, UpdateConsultant>().ReverseMap();
      CreateMap<ConsultantAddressModel, UpdateConsultantAddressModel>().ReverseMap();
      CreateMap<ConsultantDto, GetConsultantById>().ReverseMap();
      CreateMap<ConsultantAddressModel, GetConsultantAddressById>().ReverseMap();
      CreateMap<ConsultantDocumentsModel, AddConsultantDocuments>().ReverseMap();
      // Category
      CreateMap<CategoryModel, GetAllCategoryList>().ReverseMap();
      CreateMap<CategoryModel, AddCategory>().ReverseMap();
      // ContactUs
      CreateMap<ContactUsModel, AddContactUs>().ReverseMap();
      // City
      CreateMap<CityModel, GetAllCityList>().ReverseMap();
      // Customer
      CreateMap<CustomerDto, AddCustomer>().ReverseMap();
      CreateMap<CustomerAddressModel, AddCustomerAddressModel>().ReverseMap();
      CreateMap<CustomerDto, UpdateCustomer>().ReverseMap();
      CreateMap<CustomerAddressModel, UpdateCustomerAddressModel>().ReverseMap();
      CreateMap<CustomerDto, GetCustomerById>().ReverseMap();
      CreateMap<CustomerAddressModel, GetCustomerAddressById>().ReverseMap();
      // CustomerOTP
      CreateMap<CustomerOTP, AddCustomersOTP>().ReverseMap();
      CreateMap<CustomerOTP, VerifyOTP>().ReverseMap();
      CreateMap<CustomerOTP, ResendOTP>().ReverseMap();
      // BookingServiceFeedback
      CreateMap<BookingServiceFeedbackModel, AddBookingServiceFeedbackModel>().ReverseMap();
      CreateMap<BookingServiceFeedbackModel, UpdateBookingServiceFeedbackModel>().ReverseMap();
      CreateMap<BookingServiceFeedbackModel, GetBookingServiceFeedbackId>().ReverseMap();
      // Booking
      CreateMap<BookingModel, AddBooking>().ReverseMap();
      // TableOfTable
      CreateMap<TableOfTablemodel, AddTableOfTable>().ReverseMap();
      // AppoinMent
      CreateMap<AppoinMentModel, AddAppoinMent>().ReverseMap();
      CreateMap<AppoinMentModel, UpdateAppoinMent>().ReverseMap();
      CreateMap<AppoinMentModel, GetAppoinMentById>().ReverseMap();
      CreateMap<AppoinMentModel, ConfirmAppoinMent>().ReverseMap();
    }
  }
}
