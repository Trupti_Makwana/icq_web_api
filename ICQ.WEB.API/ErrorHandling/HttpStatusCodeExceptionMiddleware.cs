using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;

namespace ICQ.Web.Api.ErrorHandling
{
    public static class HttpStatusCodeExceptionMiddleware
    {
        public static System.Action<IApplicationBuilder> HttpExceptionHandling()
        {
            return options =>
            {
                options.Run(
                    async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.ContentType = context.Request.ContentType;
                        var ex = context.Features.Get<IExceptionHandlerFeature>();
                        Log.Error(ex.Error, "Exception");

                        await context.Response.WriteAsync(JsonConvert.SerializeObject(new
                        {
                            message = ex.Error?.Message
                        })).ConfigureAwait(false);
                    });
            };
        }
    }
}
