using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICQ.WEB.API.Validation
{
    public class GlobalEnglishValidation
    {
        public const string RequiredCredentials= "To log in, please enter your login and password!";
        public const string NotFoundCredentials= "Sorry, but the login and password you entered do not match our records! ";
        public const string UserNotActive= "Customer Is Not Active";
        public const string UploadDocument= "Please select document!";
        public const string UploadDocumentSize= "File size should be less than 2MB.";
        public const string PasswordNotSame= "Password in both fields must be the same!";
        public const string AlreadyUserRegisterd= "The user with the given login is already registered!";
        public const string WMSNumberIsAlreadyUsed= "This WMS number is already used.";
    }
}
