using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICQ.WEB.API.Validation
{
    public class GlobalPolishValidation
    {
        public const string RequiredCredentials= "Aby się zalogować należy podać login i hasło!";
        public const string NotFoundCredentials= "Użytkownik o podanym loginie nie jest zarejestrowany w naszej bazie!";
        public const string UserNotActive= "Klient nie jest aktywny";
        public const string UploadDocument= "Wybierz dokument";
        public const string UploadDocumentSize = "Rozmiar pliku powinien być mniejszy niż 2 MB.";
        public const string PasswordNotSame= "Hasło w obu polach musi być takie samo!";
        public const string AlreadyUserRegisterd= "Użytkownik z podanym loginem jest już zarejestrowany!";
        public const string WMSNumberIsAlreadyUsed = "Podany nr WMS jest już używany";
    }
}
