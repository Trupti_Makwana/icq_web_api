using System;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ICQ.WEB.API.Controllers
{
  [Produces("application/json")]
  [Route("AppoinMent")]
  public class AppoinMentController : Controller
  {
    private readonly IAppoinMent _objIAppoinMent;
    readonly IUnitOfWork<IDataContext> _uow;
    private readonly IMapper _mapper;
    private readonly IHostEnvironment _hostingEnv;

    public AppoinMentController(IUnitOfWork<IDataContext> uow, IAppoinMent objIAppoinMent, IMapper mapper, IHostEnvironment hostingEnv)
    {
      this._uow = uow;
      this._objIAppoinMent = objIAppoinMent;
      _mapper = mapper;
      _hostingEnv = hostingEnv;
    }

    [HttpPost]
    [Route("AddAppoinMent")]
    public IActionResult AddAppoinMent([FromBody] AddAppoinMent addAppoinMent)
    {
      var map = _mapper.Map<AppoinMentModel>(addAppoinMent);
      _uow.Context.AppoinMent.Add(map);
      _uow.Save();
      return Ok();
    }

    [HttpPost]
    [Route("UpdateAppoinMent")]
    public IActionResult UpdateAppoinMent([FromBody] UpdateAppoinMent updateAppoinMent)
    {
      var map = _mapper.Map<AppoinMentModel>(updateAppoinMent);
      _uow.Context.AppoinMent.Update(map);
      _uow.Save();
      return Ok();
    }
    [HttpGet]
    [Route("GetAppoinMentById")]
    public IActionResult GetAppoinMentById(Guid id)
    {
      return Ok(_objIAppoinMent.GetAppoinMentById(id));
    }

    [HttpDelete]
    [Route("DeleteAppoinMent")]
    public IActionResult Delete(Guid id)
    {
      var appoinment = _objIAppoinMent.Find(id);
      if (appoinment == null)
      {
        return NotFound();
      }
      _uow.Context.AppoinMent.Remove(appoinment);
      _uow.Save();
      return Ok();
    }
  }
}
