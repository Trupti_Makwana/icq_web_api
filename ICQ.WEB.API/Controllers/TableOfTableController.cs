using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ICQ.WEB.API.Controllers
{
  
    [Produces("application/json")]
    [Route("TableOfTable")]
    public class TableOfTableController : Controller
    {
       private readonly ITableOfTable _objITableOfTable;
       readonly IUnitOfWork<IDataContext> _uow;
       private readonly IMapper _mapper;

       public TableOfTableController(IUnitOfWork<IDataContext> uow, ITableOfTable objITableOfTable, IMapper mapper)
       {
         this._uow = uow;
         this._objITableOfTable = objITableOfTable;
        _mapper = mapper;
       }
       [HttpPost]
       [Route("AddTOT")]
       public IActionResult AddTOT([FromBody] AddTableOfTable addTableOfTable)
       {
          var map = _mapper.Map<TableOfTablemodel>(addTableOfTable);
         _uow.Context.TableOfTable.Add(map);
         _uow.Save();
         return Ok();
       }
  }
}
