using API.Context;
using API.Context.Interface;
using API.Core;
using API.Core.Interface;
using API.Core.Model;
using AutoMapper;
using ICQ.WEB.API.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.IO;


namespace ICQ.WEB.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Upload")]
    public class UploadController : Controller
    {
        readonly IUnitOfWork<IDataContext> _uow;
        private readonly IMapper _mapper;
        private readonly appsettings _appsettings;
        public readonly ICommon _iCommon;
    [System.Obsolete]
    private readonly IHostingEnvironment hostingEnv;
        public UploadController(IUnitOfWork<IDataContext> uow,  IMapper mapper, IOptions<appsettings> appSettings, ICommon iCommon, IHostingEnvironment hostingEnv)
        {
            this._uow = uow;
            _mapper = mapper;
            _appsettings = appSettings.Value;
            _iCommon = iCommon;
            this.hostingEnv = hostingEnv;
        }

        [HttpPost]
    [System.Obsolete]
    public IActionResult Upload()
        {
            var file = Request.Form.Files[0];
            string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            if (file == null || file.Length == 0)
            {
                ModelState.AddModelError("Message", GlobalModel.LanguageId == 1 ? GlobalEnglishValidation.UploadDocument : GlobalPolishValidation.UploadDocument);
                return new Microsoft.AspNetCore.Mvc.UnprocessableEntityObjectResult(ModelState);
            }
            if (file.Length > (2 * 1024 * 1024))
            {
                ModelState.AddModelError("Message", GlobalModel.LanguageId == 1 ? GlobalEnglishValidation.UploadDocumentSize : GlobalPolishValidation.UploadDocumentSize);
                return new Microsoft.AspNetCore.Mvc.UnprocessableEntityObjectResult(ModelState);
            }
            var uniqueFileName = $"User_{GlobalModel.UserId}_image_{_iCommon.RandomGenerator(5, saAllowedCharacters)}{Path.GetExtension(file.FileName)}";

            string path = Path.Combine(hostingEnv.WebRootPath, "Images\\ResourceImages", uniqueFileName);
            
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            var obj = new
            {
                dbPath = uniqueFileName
            };
            return Ok(obj);
                        //var folderName = Path.Combine("FMLogistic", "Images");
            //var filePath = Path.Combine(_appsettings.UploadConfiguration.ImagePath, folderName);

            //if (!Directory.Exists(filePath))
            //{
            //    Directory.CreateDirectory(filePath);
            //}
            //var dbPath = Path.Combine(filePath, uniqueFileName);

            //using (var fileStream = new FileStream(dbPath, FileMode.Create))
            //{
            //     file.CopyTo(fileStream);
            //}
        }
    }
}
