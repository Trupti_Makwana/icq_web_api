using System;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ICQ.WEB.API.Controllers
{

    [Produces("application/json")]
    [Route("Customer")]
    public class CustomerController : Controller
    {
         private readonly ICustomer _objICustomer;
         readonly IUnitOfWork<IDataContext> _uow;
         private readonly IMapper _mapper;

       public CustomerController(IUnitOfWork<IDataContext> uow, ICustomer objICustomer, IMapper mapper)
       {
         this._uow = uow;
         this._objICustomer = objICustomer;
        _mapper = mapper;
       }

       [HttpPost]
       [Route("AddCustomer")]
       public IActionResult AddCustomer([FromBody] AddCustomer addCustomer)
       {
         var map = _mapper.Map<CustomerDto>(addCustomer);
        _uow.Context.Customer.Add(map);
        _uow.Save();
         return Ok();
       }

       [HttpPost]
       [Route("UpdateCustomer")]
       public IActionResult UpdateCustomer([FromBody] UpdateCustomer UpdateCustomer)
       {
         var map = _mapper.Map<CustomerDto>(UpdateCustomer);
        _uow.Context.Customer.Update(map);
        _uow.Save();
         return Ok();
       }

       [HttpGet]
       [Route("GetCustomerById")]
       public IActionResult GetCustomerById(Guid id)
       {
         return Ok(_objICustomer.GetById(id));
       }

       [HttpGet]
       [Route("GetAllCustomerList")]
       public IActionResult GetAllCustomerList()
       {
         return Ok(_objICustomer.GetAllCustomerList());
       }

    }
}
