using API.Context.Interface;
using API.Core;
using API.Core.Interface;
using API.Core.Interface.Client_Admin;
using API.Core.Model;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace ICQ.WEB.API.Controllers
{
  [Produces("application/json")]
  [Route("ContactUs")]
  public class ContactUsController : Controller
  {
    private readonly IContactUs _iContact;
    readonly IUnitOfWork<IDataContext> _uow;
    private readonly IMapper _mapper;
    public ContactUsController(IUnitOfWork<IDataContext> uow, IContactUs iContact, IMapper mapper)
    {
      this._uow = uow;
      this._iContact = _iContact;
      _mapper = mapper;
    }

    [HttpPost]
    [Route("AddContactUs")]
    public IActionResult AddContactUs([FromBody] AddContactUs addContact)
    {
      var map = _mapper.Map<ContactUsModel>(addContact);
      _uow.Context.ContactUs.Add(map);
      _uow.Save();
      return Ok();
    }

    [HttpGet]
    [Route("SendEmailToAdmin")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public void SendEmailToAdmin(string Receiver, string Message, string name)
    {
      try
      {
        string adminbody = string.Empty;
        using (StreamReader reader = new StreamReader("./EmailTemplates/contactus_admin.html"))
        {
          adminbody = reader.ReadToEnd();
        }
        adminbody = adminbody.Replace("{Name}", name);
        adminbody = adminbody.Replace("{Message}", Message);
        adminbody = adminbody.Replace("{Email}", Receiver);

        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress("shilpa.dafada.epistic@gmail.com");
        mail.To.Add("shilpa.dafada.epistic@gmail.com");
        mail.Subject = "New inquiry from ICQ";
        mail.Body = adminbody;
        mail.IsBodyHtml = true;
        SmtpServer.UseDefaultCredentials = false;
        SmtpServer.Port = 587;
        SmtpServer.Credentials = new System.Net.NetworkCredential("shilpa.dafada.epistic@gmail.com", "epistic1011");
        SmtpServer.EnableSsl = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        SmtpServer.Send(mail);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    [HttpGet]
    [Route("SendEmailUser")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public void SendEmailUser(string Receiver, string name)
    {
      try
      {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader("./EmailTemplates/contactus_user.html"))
        {
          body = reader.ReadToEnd();
        }
        body = body.Replace("{Name}", name);

        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress("shilpa.dafada.epistic@gmail.com");
        mail.To.Add(Receiver);
        mail.Subject = "New message from ICQ";
        mail.Body = body;
        mail.IsBodyHtml = true;
        SmtpServer.Port = 587;
        SmtpServer.Credentials = new System.Net.NetworkCredential("shilpa.dafada.epistic@gmail.com", "epistic1011");
        SmtpServer.EnableSsl = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        SmtpServer.Send(mail);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}
