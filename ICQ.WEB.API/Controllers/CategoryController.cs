using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ICQ.WEB.API.Controllers
{
    [Produces("application/json")]
    [Route("Category")]
    public class CategoryController : Controller
    {
       private readonly ICategory _objICategory;
       readonly IUnitOfWork<IDataContext> _uow;
       private readonly IMapper _mapper;

       public CategoryController(IUnitOfWork<IDataContext> uow, ICategory objlcategory, IMapper mapper)
       {
        this._uow = uow;
        this._objICategory = objlcategory;
       _mapper = mapper;
       }
    
       [HttpGet]
       [Route("GetAllCategoryList")]
       public IActionResult GetAllCategoryList()
       {
         return Ok(_objICategory.GetAllCategoryList());
       }
       [HttpPost]
       [Route("AddCategory")]
       public IActionResult AddCategory([FromBody] AddCategory addcategory)
       {
         var map = _mapper.Map<CategoryModel>(addcategory);
        _uow.Context.Category.Add(map);
        _uow.Save();
         return Ok();
       }


  }
}
