using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace ICQ.WEB.API.Controllers
{
  [Produces("application/json")]
  [Route("Consultant")]
  public class DocumentController : Controller
  {
    private readonly IConsultant _objICunsultant;
    readonly IUnitOfWork<IDataContext> _uow;
    private readonly IMapper _mapper;
    private readonly IHostEnvironment _hostingEnv;

    public DocumentController(IUnitOfWork<IDataContext> uow, IConsultant objIConsultant, IMapper mapper, IHostEnvironment hostingEnv)
    {
      this._uow = uow;
      this._objICunsultant = objIConsultant;
      _mapper = mapper;
      _hostingEnv = hostingEnv;
    }

    [HttpPost]
    
    [Route("Document")]
    public async Task<IActionResult> Document(ICollection<IFormFile> files)
    {
      //var map = _mapper.Map<ConsultantDocumentsModel>(addDocument);
      //_uow.Context.ConsultantDocuments.Add(map);
      //_uow.Save();
      return Ok();
    }
  }
}
