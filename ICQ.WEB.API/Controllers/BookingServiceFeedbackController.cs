using System;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace ICQ.WEB.API.Controllers
{
  [Produces("application/json")]
  [Route("Reviews")]
  public class BookingServiceFeedbackController : Controller
  {
    private readonly IBookingServiceFeedback _objIBookingServiceFeedback;
    readonly IUnitOfWork<IDataContext> _uow;
    private readonly IMapper _mapper;

    public BookingServiceFeedbackController(IUnitOfWork<IDataContext> uow, IBookingServiceFeedback objIBookingServiceFeedback, IMapper mapper)
    {
      this._uow = uow;
      this._objIBookingServiceFeedback = objIBookingServiceFeedback;
      _mapper = mapper;
    }

    [HttpPost]
    [Route("AddReviews")]
    public IActionResult AddReviews([FromBody] AddBookingServiceFeedbackModel addBookingServiceFeedbackModel)
    {
      var map = _mapper.Map<BookingServiceFeedbackModel>(addBookingServiceFeedbackModel);
      _uow.Context.BookingServiceFeedback.Add(map);
      _uow.Save();
      return Ok();
    }

    [HttpPost]
    [Route("UpdateReviews")]
    public IActionResult UpdateReviews([FromBody] UpdateBookingServiceFeedbackModel updateBookingServiceFeedbackModel)
    {
      var map = _mapper.Map<BookingServiceFeedbackModel>(updateBookingServiceFeedbackModel);
      _uow.Context.BookingServiceFeedback.Update(map);
      _uow.Save();
      return Ok();
    }

    [HttpGet]
    [Route("GetReviewsId")]
    public IActionResult GetReviewsId(Guid id)
    {
      return Ok(_objIBookingServiceFeedback.GetById(id));
    }

    [HttpGet]
    [Route("GetAllReviewsList")]
    public IActionResult GetAllReviewsList()
    {
      return Ok(_objIBookingServiceFeedback.GetAllBookingServiceFeedbackList());
    }
  }
}
