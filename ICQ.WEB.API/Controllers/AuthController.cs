using API.Context.Interface;
using API.Core;
using API.Core.Interface;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Client_Admin
{
  [Produces("application/json")]
  [Route("Auth")]

  public class AuthController : Controller
  {
    private readonly ICustomerOTP ICustomerOTP;
    private readonly IMapper _mapper;
    readonly IUnitOfWork<IDataContext> _uow;
    public AuthController(IUnitOfWork<IDataContext> uow, ICustomerOTP ICustomerOTP, IMapper mapper)
    {
      this._uow = uow;
      this.ICustomerOTP = ICustomerOTP;
      _mapper = mapper;
    }

    [HttpPost]
    [Route("GenerateOTP")]
    public IActionResult GenerateOTP([FromBody] AddCustomersOTP CustomersOTP)
    {
      var map = _mapper.Map<AddCustomersOTP>(CustomersOTP);
      string[] data = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
      string a = ICustomerOTP.GenerateOTP(CustomersOTP, 4, data);
      map.OTP = a;
      //_uow.Context.CustomersOTP.Add(map);
      //_uow.Save();
      return Ok(map);
    }

    [HttpPost]
    [Route("VerifyOTP")]
    public CustomerDto VerifyOTP([FromBody] VerifyOTP VerifyOTP)
    {
      CustomerDto isVerified = ICustomerOTP.VerifyOTP(VerifyOTP);
      return isVerified;
    }

    [HttpPost]
    [Route("ResendOTP")]
    public IActionResult ResendOTP([FromBody] ResendOTP ResendOTP)
    {
      CustomerOTP obj = ICustomerOTP.ResendOTP(ResendOTP);
      var map = _mapper.Map<CustomerOTP>(obj);
      _uow.Context.CustomersOTP.Update(map);
      return Ok();
    }
  }
}
