using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ICQ.WEB.API.Controllers
{

    [Produces("application/json")]
    [Route("Booking")]
    public class BookingController : Controller
    {
       private readonly IBooking _objIBooking;
       readonly IUnitOfWork<IDataContext> _uow;
       private readonly IMapper _mapper;

        public BookingController(IUnitOfWork<IDataContext> uow, IBooking objlIBooking, IMapper mapper)
        {
          this._uow = uow;
          this._objIBooking = objlIBooking;
         _mapper = mapper;
        }

        //[HttpPost]
        //[Route("AddBooking")]
        //public IActionResult AddReviews([FromBody] AddBooking addBooking)
        //{
        //  var map = _mapper.Map<BookingModel>(addBooking);
        //  _uow.Context.Booking.Add(map);
        //  _uow.Save();
        //  return Ok();
        //}
    }
}
