using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using API.Context.Interface;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model.Client_Admin;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using System.Linq;

namespace WebApi.Controllers.Client_Admin
{
  [Produces("application/json")]
  [Route("Consultant")]
  public class ConsultantController : Controller
  {
    private readonly IConsultant _objICunsultant;
    readonly IUnitOfWork<IDataContext> _uow;
    private readonly IMapper _mapper;
    private readonly IHostEnvironment _hostingEnv;
    public ConsultantController(IUnitOfWork<IDataContext> uow, IConsultant objIConsultant, IMapper mapper, IHostEnvironment hostingEnv)
    {
      this._uow = uow;
      this._objICunsultant = objIConsultant;
      _mapper = mapper;
      _hostingEnv = hostingEnv;
    }

    [HttpPost]
    [Route("AddConsultant")]
    public IActionResult AddConsultant([FromForm]AddConsultant addConsultant)
    {
      List<TableOfTablemodel> listtot = new List<TableOfTablemodel>();
      TableOfTablemodel tot = new TableOfTablemodel();

      var TotList = (from r in _uow.Context.TableOfTable
                 select r).ToList();

      List<AddConsultantDocuments> TotValue = new List<AddConsultantDocuments>();

      for (int i = 0; i < TotList.Count; i++)
      {
        if(TotList[i].TotValue=="CV")
        {
          if (addConsultant.CV != null)
          {
            AddConsultantDocuments objCV = new AddConsultantDocuments();
            objCV.ConsultantId = null;
            objCV.TotId = TotList[i].TotId;
            objCV.FileType = Guid.NewGuid();

            var uniquefilename = $"user_image_{addConsultant.CV.FileName}";
            
            objCV.FilePath = Path.Combine(_hostingEnv.ContentRootPath, "wwwroot\\images\\resourceImages", uniquefilename); 
            objCV.DocumentName = TotList[i].TotValue;
            TotValue.Add(objCV);
          }
        }
        else if (TotList[i].TotValue == "DrivingLicence")
        {
          if (addConsultant.DrivingLicence != null)
          {
            AddConsultantDocuments objDrivingLicence = new AddConsultantDocuments();
            objDrivingLicence.ConsultantId = null;
            objDrivingLicence.TotId = TotList[i].TotId;
            objDrivingLicence.FileType = Guid.NewGuid();
            var uniquefilename = $"user_image_{addConsultant.DrivingLicence.FileName}";
            objDrivingLicence.FilePath = Path.Combine(_hostingEnv.ContentRootPath, "wwwroot\\images\\resourceImages", uniquefilename); ;
            objDrivingLicence.DocumentName = TotList[i].TotValue;
            TotValue.Add(objDrivingLicence);
          }
        }
        else if (TotList[i].TotValue == "ProofofAddress")
        {
          if (addConsultant.ProofofAddress != null)
          {
            AddConsultantDocuments objProofofAddress = new AddConsultantDocuments();
            objProofofAddress.ConsultantId = null;
            objProofofAddress.TotId = TotList[i].TotId;
            objProofofAddress.FileType = Guid.NewGuid();
            var uniquefilename = $"user_image_{addConsultant.ProofofAddress.FileName}";
            objProofofAddress.FilePath = Path.Combine(_hostingEnv.ContentRootPath, "wwwroot\\images\\resourceImages", uniquefilename);  
            objProofofAddress.DocumentName = TotList[i].TotValue;
            TotValue.Add(objProofofAddress);
          }
        }
        else if (TotList[i].TotValue == "DBSCertificate")
        {
          if (addConsultant.DBSCertificate != null)
          {
            AddConsultantDocuments objDBSCertificate = new AddConsultantDocuments();
            objDBSCertificate.ConsultantId = null;
            objDBSCertificate.TotId = TotList[i].TotId;
            objDBSCertificate.FileType = Guid.NewGuid();
            var uniquefilename = $"user_image_{addConsultant.DBSCertificate.FileName}";
            objDBSCertificate.FilePath = Path.Combine(_hostingEnv.ContentRootPath, "wwwroot\\images\\resourceImages", uniquefilename);
            objDBSCertificate.DocumentName = TotList[i].TotValue;
            TotValue.Add(objDBSCertificate);
          }
        }

        else if (TotList[i].TotValue == "PassPort")
        {
          if (addConsultant.PassPort != null)
          {
            AddConsultantDocuments objPassPort = new AddConsultantDocuments();
            objPassPort.ConsultantId = null;
            objPassPort.TotId = TotList[i].TotId;
            objPassPort.FileType = Guid.NewGuid();
            var uniquefilename = $"user_image_{addConsultant.PassPort.FileName}";
            objPassPort.FilePath = Path.Combine(_hostingEnv.ContentRootPath, "wwwroot\\images\\resourceImages", uniquefilename);
            objPassPort.DocumentName = TotList[i].TotValue;
            TotValue.Add(objPassPort);
          }
        }
      }
     
     

      List<FileList> listoffile = new List<FileList>();
      FileList obj1 = new FileList();
      obj1.Files = addConsultant.CV;
      listoffile.Add(obj1);

      FileList obj2 = new FileList();
      obj2.Files = addConsultant.DrivingLicence;
      listoffile.Add(obj2);

      FileList obj3 = new FileList();
      obj3.Files = addConsultant.DBSCertificate;
      listoffile.Add(obj3);

      FileList obj4 = new FileList();
      obj4.Files = addConsultant.ProofofAddress;
      listoffile.Add(obj4);

      FileList obj5 = new FileList();
      obj5.Files = addConsultant.PassPort;
      listoffile.Add(obj5);



      //List<AddConsultantDocuments> list = new List<AddConsultantDocuments>();
      //AddConsultantDocuments objcv = new AddConsultantDocuments();
      //objcv.CV = addConsultant.CV;
      //list.Add(objcv);

      //AddConsultantDocuments objdrivinglicence = new AddConsultantDocuments();
      //objdrivinglicence.DrivingLicence = addConsultant.DrivingLicence;
      
      //list.Add(objdrivinglicence);

      //AddConsultantDocuments objdBScertificate = new AddConsultantDocuments();
      
      //objdBScertificate.DBSCertificate = addConsultant.DBSCertificate;
      
      //list.Add(objdBScertificate);

      //AddConsultantDocuments objproofadd = new AddConsultantDocuments();
      //objproofadd.ProofofAddress = addConsultant.ProofofAddress;
      
      //list.Add(objproofadd);

      //AddConsultantDocuments objpassport = new AddConsultantDocuments();
      //objpassport.PassPort = addConsultant.PassPort;
      
      //list.Add(objpassport);
      addConsultant.ConsultantDocuments = TotValue;

      // Upload File to folder
      foreach (var item in listoffile)
      {
        string path = "";
        if (item.Files != null)
        {
          if (item.Files.Length > 0)
          {
            if (item == null || item.Files.Length == 0)
            {
              ModelState.AddModelError("message", "file not found");
              return new Microsoft.AspNetCore.Mvc.UnprocessableEntityObjectResult(ModelState);
            }
            if (item.Files.Length > (1 * 1024 * 1024))
            {
              ModelState.AddModelError("message", "maximum 1 mb allowed");
              return new Microsoft.AspNetCore.Mvc.UnprocessableEntityObjectResult(ModelState);
            }
            var uniquefilename = $"user_image_{item.Files.FileName}";
            path = Path.Combine(_hostingEnv.ContentRootPath, "wwwroot\\images\\resourceImages", uniquefilename);
            using (var filestream = new FileStream(path, FileMode.Create))
            {
              item.Files.CopyTo(filestream);
            }
            var Obj = new
            {
              dbpath = uniquefilename
            };
          }
        }

      }
      List<AddConsultantAddressModel> AddressList = new List<AddConsultantAddressModel>();
      AddConsultantAddressModel obj = new AddConsultantAddressModel
      {
        ConsultantId = null,
        IsDefaultAddress = true,
        Address = addConsultant.Address,
        ZipCode = addConsultant.ZipCode,
        Latitude = addConsultant.Latitude,
        Longitude = addConsultant.Longitude,
        UrlId = addConsultant.UrlId,
        IsActive = true
      };
      AddressList.Add(obj);
      addConsultant.ConsultantAddress = AddressList;
      var map = _mapper.Map<ConsultantDto>(addConsultant);
      map.IsActive = true;
      map.CreatedOn = DateTime.Now;
      map.IsAvailable = true;
      map.IsApproved = false;
      map.DateofBirth = Convert.ToDateTime(map.DateofBirth);
      map.ApprovedDate = null;
      _uow.Context.Consultant.Add(map);
      _uow.Save();
      return Ok();
    }


    [HttpPost]
    [Route("UpdateConsultant")]
    public IActionResult UpdateConsultant([FromBody] UpdateConsultant updateConsultant)
    {
      var map = _mapper.Map<ConsultantDto>(updateConsultant);
      _uow.Context.Consultant.Update(map);
      _uow.Save();
      return Ok();
    }
    [HttpGet]
    [Route("GetConsultantById")]
    public IActionResult GetConsultantById(Guid id)
    {
      return Ok(_objICunsultant.GetById(id));
    }
    [HttpGet]
    [Route("GetAllConsultantList")]
    public IActionResult GetAllConsultantList()
    {
      return Ok(_objICunsultant.GetallConsultantList());
    }

    [HttpGet]
    [Route("RegisterSendEmailUser")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public void SendEmailUser(string Receiver, string name)
    {
      try
      {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader("./EmailTemplates/email_templates.html"))
        {
          body = reader.ReadToEnd();
        }
        body = body.Replace("{LastName}", name);

        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress("noreply@icarequick.com");
        mail.To.Add(Receiver);
        mail.Subject = "DO NOT REPLY- ICQ WEB APPLICATION";
        mail.Body = body;
        mail.IsBodyHtml = true;
        SmtpServer.Port = 587;
        SmtpServer.Credentials = new System.Net.NetworkCredential("noreply@icarequick.com", "Epistic@#1011");
        SmtpServer.EnableSsl = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        SmtpServer.Send(mail);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    [HttpGet]
    [Route("RegisterSendEmailToAdmin")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public void SendEmailToAdmin(string Receiver, string Message, string name)
    {
      try
      {
        string adminbody = string.Empty;
        using (StreamReader reader = new StreamReader("./EmailTemplates/Register.html"))
        {
          adminbody = reader.ReadToEnd();
        }
        adminbody = adminbody.Replace("{LastName}", name);
        adminbody = adminbody.Replace("{PhoneNo}", Message);
        adminbody = adminbody.Replace("{Email}", Receiver);

        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress("noreply@icarequick.com");
        mail.To.Add("office@icarequick.com");
        mail.Subject = "New inquiry from ICQ";
        mail.Body = adminbody;
        mail.IsBodyHtml = true;
        SmtpServer.Port = 587;
        SmtpServer.Credentials = new System.Net.NetworkCredential("noreply@icarequick.com ", "Epistic@#1011");
        SmtpServer.EnableSsl = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        SmtpServer.Send(mail);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

  }
}
