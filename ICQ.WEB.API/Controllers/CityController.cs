using System;
using API.Context.Interface;
using API.Context.Pagination;
using API.Core;
using API.Core.Interface.Client_Admin;
using API.Core.Model;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;


namespace ICQ.WEB.API.Controllers
{

    [Produces("application/json")]
    [Route("City")]
    public class CityController : Controller
    {
       private readonly ICity _ICity;
       readonly IUnitOfWork<IDataContext> _uow;
       private readonly IMapper _mapper;

        public CityController(IUnitOfWork<IDataContext> uow, ICity ObjICity, IMapper mapper)
        {
          this._uow = uow;
          this._ICity = ObjICity;
         _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAllCities")]
        public IActionResult GetAllCityList()
        {
         return Ok(_ICity.GetCityList());
        }
   }
}
