using System;
using System.Collections.Generic;
using API.Context.Interface;
using API.Core.Model.Client_Admin;

namespace API.Core.Interface.Client_Admin
{
  public interface IBookingServiceFeedback : IEntityRepository<BookingServiceFeedbackModel>
  {
    GetBookingServiceFeedbackId GetById(Guid id);
    List<GetAllBookingServiceFeedbackList> GetAllBookingServiceFeedbackList();
  }
}
