using System;
using System.Collections.Generic;
using API.Context.Interface;
using API.Core.Model.Client_Admin;

namespace API.Core.Interface.Client_Admin
{
    public interface ICustomer : IEntityRepository<CustomerDto>
    {
       GetCustomerById GetById(Guid id);
       List<GetAllCustomerList> GetAllCustomerList();
  }
}
