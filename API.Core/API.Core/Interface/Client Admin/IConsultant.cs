using System;
using System.Collections.Generic;
using API.Context.Interface;
using API.Core.Model.Client_Admin;

namespace API.Core.Interface.Client_Admin
{
  public interface IConsultant : IEntityRepository<ConsultantDto>
  {
      GetConsultantById GetById(Guid id);
      List<GetAllConsultantList> GetallConsultantList();
  }
}
