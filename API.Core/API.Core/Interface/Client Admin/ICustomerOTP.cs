using API.Context.Interface;
using API.Core.Model.Client_Admin;


namespace API.Core.Interface
{
  public interface ICustomerOTP : IEntityRepository<CustomerOTP>
  {
    string GenerateOTP(AddCustomersOTP request, int iOTPLength, string[] saAllowedCharacters);
    CustomerDto VerifyOTP(VerifyOTP request);
    CustomerOTP ResendOTP(ResendOTP request);

  }
}
