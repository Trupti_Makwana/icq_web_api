using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Context.Interface;
using API.Core.Model.Client_Admin;

namespace API.Core.Interface.Client_Admin
{
     public interface IContactUs : IEntityRepository<ContactUsModel>
     {
     }
}
