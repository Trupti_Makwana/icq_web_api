using System;
using API.Context.Interface;
using API.Core.Model.Client_Admin;


namespace API.Core.Interface.Client_Admin
{
  public interface ICustomerAddress : IEntityRepository<CustomerAddressModel>
  {
  }
}
