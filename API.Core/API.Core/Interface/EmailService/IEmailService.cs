﻿using System;
using System.Collections.Generic;
using API.Context.Interface;
using API.Context.Pagination;
using API.Core.Model;
using API.Core.Model.EmailService;

namespace API.Core.Interface
{
    public interface IEmailService : IEntityRepository<Message>
    {
        void SendEmail(Message message);
    }
}
