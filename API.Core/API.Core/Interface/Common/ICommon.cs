using System;
using System.Collections.Generic;
using API.Context.Interface;
using API.Context.Pagination;
using API.Core.Model;
using API.Core.Model.Common;
using API.Core.Model.EmailService;

namespace API.Core.Interface
{
    public interface ICommon : IEntityRepository<Common>
    {
        string RandomGenerator(int iOTPLength, string[] saAllowedCharacters);
    }
}
