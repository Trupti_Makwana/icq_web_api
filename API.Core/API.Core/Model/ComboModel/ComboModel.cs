﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Model.ComboModel
{
    public class ComboModel
    {
        public int KeyId { get; set; }
        public string KeyValue { get; set; }
    }
}
