﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Core.Model
{
    public class appsettings
    {
        public JWT JWT { get; set; }
        public EmailConfiguration EmailConfiguration { get; set; }
        public UploadConfiguration UploadConfiguration { get; set; }
    }
    public class JWT
    {
        public string issuer { get; set; }
        public string audience { get; set; }
        public int expires { get; set; }
        public string secretKey { get; set; }
    }
    public class EmailConfiguration
    {
        public string From { get; set; }
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FrontUrl { get; set; }
    }
    public class UploadConfiguration
    {
        public string ImagePath { get; set; }
    }
}