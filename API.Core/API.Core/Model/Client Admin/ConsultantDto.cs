using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using API.Context;
using Microsoft.AspNetCore.Http;

namespace API.Core.Model.Client_Admin
{
  public partial class ConsultantDto : BaseEntity
  {
    public ConsultantDto()
    {
      ConsultantAddress = new List<ConsultantAddressModel>();
      ConsultantDocuments = new List<ConsultantDocumentsModel>();
    }
    [Key]
    public Guid ConsultantId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string PhoneNo { get; set; }
    public string AltPhoneNo { get; set; }
    public string Gender { get; set; }
    public Guid CategoryId { get; set; }
    public DateTime DateofBirth { get; set; }
    public string ProfilePic { get; set; }
    public string AboutMe { get; set; }
    public string BasicPrice { get; set; }
    public string DBSRefNumber { get; set; }
    public bool IsApproved { get; set; }
    public DateTime? ApprovedDate { get; set; }
    public string ApprovedBy { get; set; }
    public bool IsAvailable { get; set; }
    public IList<ConsultantAddressModel> ConsultantAddress { get; set; }
    public IList<ConsultantDocumentsModel> ConsultantDocuments { get; set; }
  }

  public partial class AddConsultant : BaseEntity
  {
    public AddConsultant()
    {
      ConsultantAddress = new List<AddConsultantAddressModel>();
      ConsultantDocuments = new List<AddConsultantDocuments>();

    }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string PhoneNo { get; set; }
    public string AltPhoneNo { get; set; }
    public string Gender { get; set; }
    public DateTime DateofBirth { get; set; }
    public string ProfilePic { get; set; }
    public string AboutMe { get; set; }
    public string BasicPrice { get; set; }
    public string DBSRefNumber { get; set; }
    public bool? IsApproved { get; set; }
    public DateTime? ApprovedDate { get; set; }
    public string ApprovedBy { get; set; }
    public bool? IsAvailable { get; set; }
    public Guid CategoryId { get; set; }

    public string Address { get; set; }
    public string ZipCode { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
    public bool IsDefaultAddress { get; set; }
    public string UrlId { get; set; }

    public string DocumentName { get; set; }
    public Guid? TotId { get; set; }
    public Guid? FileType { get; set; }
    public string FilePath { get; set; }
    public IFormFile Files { get; set; }
    public IFormFile CV { get; set; }
    public IFormFile DrivingLicence { get; set; }
    public IFormFile ProofofAddress { get; set; }
    public IFormFile DBSCertificate { get; set; }
    public IFormFile PassPort { get; set; }
    public IList<AddConsultantAddressModel> ConsultantAddress { get; set; }
    public IList<AddConsultantDocuments> ConsultantDocuments { get; set; }
  }

  public partial class UpdateConsultant : BaseEntity
  {
    public UpdateConsultant()
    {
      ConsultantAddress = new List<UpdateConsultantAddressModel>();
    }
    public Guid ConsultantId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string PhoneNo { get; set; }
    public string AltPhoneNo { get; set; }
    public string Gender { get; set; }
    public DateTime DateofBirth { get; set; }
    public string ProfilePic { get; set; }
    public string AboutMe { get; set; }
    public string BasicPrice { get; set; }
    public string DBSRefNumber { get; set; }
    public bool IsApproved { get; set; }
    public DateTime ApprovedDate { get; set; }
    public Guid TotId { get; set; }
    public string ApprovedBy { get; set; }
    public bool IsAvailable { get; set; }
    public Guid? CategoryId { get; set; }
    public IList<UpdateConsultantAddressModel> ConsultantAddress { get; set; }
  }

  public class GetConsultantById
  {
    public Guid ConsultantId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string PhoneNo { get; set; }
    public string AltPhoneNo { get; set; }
    public string Gender { get; set; }
    public DateTime DateofBirth { get; set; }
    public string ProfilePic { get; set; }
    public bool IsApproved { get; set; }
    public DateTime ApprovedDate { get; set; }
    public Guid TotId { get; set; }
    public string ApprovedBy { get; set; }
    public bool IsAvailable { get; set; }
    public Guid? CategoryId { get; set; }
    public IList<GetConsultantAddressById> ConsultantAddress { get; set; }
  }

  public class GetAllConsultantList
  {
    public Guid ConsultantId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string PhoneNo { get; set; }
    public string AltPhoneNo { get; set; }
    public string Gender { get; set; }
    public DateTime DateofBirth { get; set; }
    public Guid CategoryId { get; set; }
    public IList<ConsultantAddressModel> ConsultantAddress { get; set; }
  }
}
