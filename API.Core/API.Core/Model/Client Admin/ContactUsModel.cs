using API.Context;
using API.Context.Pagination;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Model.Client_Admin
{
  public class ContactUsModel
  {
    [Key]
    public Guid PKID { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string Message { get; set; }
    public bool IsDeleted { get; set; }

  }
  public class AddContactUs
  {
    public string Name { get; set; }
    public string Email { get; set; }
    public string Message { get; set; }
    public bool IsDeleted { get; set; }
  }

}
