using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Model.Client_Admin
{
  public class Auth 
  {
    public class GenerateOTPReturnModel
    {
      public string Token { get; set; }
      public Guid UserId { get; set; }
      public string Message { get; set; }
      public int StatusCode { get; set; }
      public string UserName { get; set; }
    }

    public class ResendOTPReturnModel
    {
      public string Token { get; set; }
      public Guid UserId { get; set; }
      public string Message { get; set; }
      public int StatusCode { get; set; }
      public string UserName { get; set; }
    }

    public class ResendOTP
    {
      public string PhoneNo { get; set; }
      public string OTP { get; set; }
    }
  }
}
