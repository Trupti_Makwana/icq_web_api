using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Model.Client_Admin
{
    public class CustomerAddressModel
    {
      [Key]
      public Guid? CustomerAddressId { get; set; }
      public Guid CustomerId { get; set; }
      public Guid? CityId { get; set; }
      public string Address { get; set; }
      public string ZipCode { get; set; }
      public decimal? Latitude { get; set; }
      public decimal? Longitude { get; set; }
      public bool IsDefaultAddress { get; set; }
      public CustomerDto Customer { get; set; }

    }
    public class AddCustomerAddressModel
    {
      public Guid CustomerId { get; set; }
      // public Guid CityId { get; set; }
      public string Address { get; set; }
      public string ZipCode { get; set; }
      public decimal? Latitude { get; set; }
      public decimal? Longitude { get; set; }
      public bool IsDefaultAddress { get; set; }
    }
    public class UpdateCustomerAddressModel
    {
      public Guid? CustomerAddressId { get; set; }
      public Guid CustomerId { get; set; }
      public string Address { get; set; }
      public string ZipCode { get; set; }
      public decimal? Latitude { get; set; }
      public decimal? Longitude { get; set; }
      public bool IsDefaultAddress { get; set; }
    }
    public class GetCustomerAddressById
    {
      public Guid? CustomerAddressId { get; set; }
      public Guid? CustomerId { get; set; }
      public string Address { get; set; }
      public string ZipCode { get; set; }
      public decimal? Latitude { get; set; }
      public decimal? Longitude { get; set; }
      public bool IsDefaultAddress { get; set; }
    }

}
