using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Model.Client_Admin
{
    public class BookingModel
    {
     [Key]
      public Guid? BookingId { get; set; }
      public Guid CustomerId { get; set; }
      public string BookingNo { get; set; }
      public DateTime BookingDate { get; set; }
      public Guid CustomerAddressId { get; set; }
      public Guid CouponCode { get; set; }
      public string CouponCodeName { get; set; }
      public decimal? CounponCodePercentage { get; set; }
      public decimal? OrderAmount { get; set; }
      public decimal? DiscountAmount { get; set; }
      public decimal? TaxAmount { get; set; }
      public decimal? OrderFinalAmount { get; set; }
      public bool IsPaymentCompleted { get; set; }
    //public Guid TotId { get; set; }
    }

    public class AddBooking
    {
      public Guid CustomerId { get; set; }
      public string BookingNo { get; set; }
      public DateTime BookingDate { get; set; }
      public Guid CustomerAddressId { get; set; }
      public Guid CouponCode { get; set; }
      public string CouponCodeName { get; set; }
      public decimal? CounponCodePercentage { get; set; }
      public decimal? OrderAmount { get; set; }
      public decimal? DiscountAmount { get; set; }
      public decimal? TaxAmount { get; set; }
      public decimal? OrderFinalAmount { get; set; }
      public bool IsPaymentCompleted { get; set; }
   // public Guid TotId { get; set; }
    }
}
