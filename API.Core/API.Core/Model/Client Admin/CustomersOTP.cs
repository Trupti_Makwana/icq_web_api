using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace API.Core.Model.Client_Admin
{
  public class CustomerOTP
  {
    [Key]
    public Guid CustomersOTPId { get; set; }
    public Guid CustomerId { get; set; }
    public string OTP { get; set; }
    public CustomerDto Customer { get; set; }
  }

  public class AddCustomersOTP
  {
    public string PhoneNo { get; set; }
    public string OTP { get; set; }
    //public Guid CustomerId { get; set; }
  }

  public class VerifyOTP
  {
    public string PhoneNo { get; set; }
    public string OTP { get; set; }
  }

  public class ResendOTP
  {
    public string PhoneNo { get; set; }
  }
}
