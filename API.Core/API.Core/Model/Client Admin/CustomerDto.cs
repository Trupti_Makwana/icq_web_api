using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.Core.Model.Client_Admin
{
  public partial class CustomerDto
  {
    public CustomerDto()
    {
      CustomersOTP = new List<CustomerOTP>();
      CustomerAddress = new List<CustomerAddressModel>();
    }
    [Key]
    public Guid CustomerId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string CountryCode { get; set; }
    public string PhoneNo { get; set; }
    public string ProfilePic { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public IList<CustomerOTP> CustomersOTP { get; set; }
    public IList<CustomerAddressModel> CustomerAddress { get; set; }
  }
  public partial class AddCustomer
  {
    public AddCustomer()
    {
      CustomersOTP = new List<CustomerOTP>();
      CustomerAddress = new List<AddCustomerAddressModel>();
    }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string CountryCode { get; set; }
    public string PhoneNo { get; set; }
    public string ProfilePic { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public IList<CustomerOTP> CustomersOTP { get; set; }
    public IList<AddCustomerAddressModel> CustomerAddress { get; set; }
  }


  public partial class UpdateCustomer
  {
    public UpdateCustomer()
    {
      CustomerAddress = new List<UpdateCustomerAddressModel>();
    }
    public Guid? CustomerId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string CountryCode { get; set; }
    public string PhoneNo { get; set; }
    public string ProfilePic { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public IList<UpdateCustomerAddressModel> CustomerAddress { get; set; }
  }

  public class GetCustomerById
  {
    public Guid CustomerId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string CountryCode { get; set; }
    public string PhoneNo { get; set; }
    public string ProfilePic { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public IList<GetCustomerAddressById> CustomerAddress { get; set; }
  }
  public class GetAllCustomerList
  {
    public Guid CustomerId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string CountryCode { get; set; }
    public string PhoneNo { get; set; }
    public string ProfilePic { get; set; }
    public IList<CustomerAddressModel> CustomerAddress { get; set; }
  }

  public class CustomerVerifyModel
  {
    public string PhoneNo { get; set; }
    public string OTP { get; set; }
  }
}
