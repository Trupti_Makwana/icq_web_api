using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Context;

namespace API.Core.Model.Client_Admin
{
  public class ConsultantAddressModel : BaseEntity
  {
    [Key]
    public Guid ConsultantAddressId { get; set; }
    public Guid ConsultantId { get; set; }
    public string Address { get; set; }
    public string ZipCode { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
    public bool IsDefaultAddress { get; set; }
    public string UrlId { get; set; }
    public ConsultantDto Consultant { get; set; }
    // public Guid CountryId { get; set; }
    // public Guid StateId { get; set; }
    // public Guid CityId { get; set; }
  }
  public class AddConsultantAddressModel : BaseEntity
  {
    public Guid? ConsultantId { get; set; }
    public string Address { get; set; }
    public string ZipCode { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
    public bool IsDefaultAddress { get; set; }
    public string UrlId { get; set; }
    // public Guid CountryId { get; set; }
    // public Guid StateId { get; set; }
    // public Guid CityId { get; set; }
  }
  public class UpdateConsultantAddressModel : BaseEntity
  {
    public Guid ConsultantAddressId { get; set; }
    public Guid ConsultantId { get; set; }
    public string Address { get; set; }
    public string ZipCode { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
    public bool IsDefaultAddress { get; set; }

    // public Guid CountryId { get; set; }
    // public Guid StateId { get; set; }
    // public Guid CityId { get; set; }
  }
  public class GetConsultantAddressById
  {
    public Guid ConsultantAddressId { get; set; }
    public Guid ConsultantId { get; set; }
    public string Address { get; set; }
    public string ZipCode { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
    public bool IsDefaultAddress { get; set; }

    // public Guid CountryId { get; set; }
    // public Guid StateId { get; set; }
    // public Guid CityId { get; set; }
  }
}




