using API.Context;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Model.Client_Admin
{
  public class ConsultantDocumentsModel : BaseEntity
  {
    [Key]
    public Guid ConsultantDocumentsId { get; set; }
    public Guid? TotId { get; set; }
    public Guid ConsultantId { get; set; }
    public string DocumentName { get; set; }
    public Guid FileType { get; set; }
    public string FilePath { get; set; }
    public ConsultantDto Consultant { get; set; }
  }
  public class AddConsultantDocuments 
  {
    [Key]
    public Guid? ConsultantId { get; set; }
    public string DocumentName { get; set; }
    public Guid? TotId { get; set; }
    public Guid? FileType { get; set; }
    public string FilePath { get; set; }
    public IFormFile Files { get; set; }
    public List<FileList> Fileslist { get; set; }
    public IFormFile CV { get; set; }
    public IFormFile DrivingLicence { get; set; }
    public IFormFile ProofofAddress { get; set; }
    public IFormFile DBSCertificate { get; set; }
    public IFormFile PassPort { get; set; }

  }
  public class FileList
  {
    public IFormFile Files { get; set; }
  }

}
