using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Model.Client_Admin
{

     public class CityModel
     {
       [Key]
       public Guid CityId { get; set; }
       public string CityName { get; set; }
     }
     public class GetAllCityList
     {
       public Guid CityId { get; set; }
       public string CityName { get; set; }
      
     }
}
