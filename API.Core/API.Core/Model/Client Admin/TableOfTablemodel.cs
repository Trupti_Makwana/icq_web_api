using System;
using System.ComponentModel.DataAnnotations;
using API.Context;

namespace API.Core.Model.Client_Admin
{
    public class TableOfTablemodel : BaseEntity
    {
      [Key]
       public Guid TotId { get; set; }
       public string TotType { get; set; }
       public string TotKey { get; set; }
       public string TotValue { get; set; }
    }
    public class AddTableOfTable : BaseEntity
    {
      public string TotType { get; set; }
      public string TotKey { get; set; }
      public string TotValue { get; set; }
    }
}
