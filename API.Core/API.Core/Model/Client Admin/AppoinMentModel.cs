using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Context;

namespace API.Core.Model.Client_Admin
{
public  class AppoinMentModel : BaseEntity
  {
    [Key]
    public Guid AppointmentId { get; set; }
    public DateTime AppointmentDate { get; set; }
    public DateTime AppointmentTime { get; set; }
    public Guid ConsultantId { get; set; }
    public Guid CustomerId { get; set; }
    public Guid BookingServiceFeedbackId { get; set; }
    public bool IsAccept { get; set; }
  }

  public class AddAppoinMent : BaseEntity
  {
    public DateTime AppointmentDate { get; set; }
    public DateTime AppointmentTime { get; set; }
    public Guid ConsultantId { get; set; }
    public Guid CustomerId { get; set; }
    public Guid BookingServiceFeedbackId { get; set; }
    public bool IsAccept { get; set; }
  }
  public class UpdateAppoinMent : BaseEntity
  {
    public Guid AppointmentId { get; set; }
    public DateTime AppointmentDate { get; set; }
    public DateTime AppointmentTime { get; set; }
    public Guid ConsultantId { get; set; }
    public Guid CustomerId { get; set; }
    public Guid BookingServiceFeedbackId { get; set; }
    public bool IsAccept { get; set; }
  }
  public class GetAppoinMentById
  {
    public Guid AppointmentId { get; set; }
    public DateTime AppointmentDate { get; set; }
    public DateTime AppointmentTime { get; set; }
    public Guid ConsultantId { get; set; }
    public Guid CustomerId { get; set; }
    public Guid BookingServiceFeedbackId { get; set; }
    public bool IsAccept { get; set; }
    public IList<GetConsultantById> Consultant { get; set; }
    public IList<GetCustomerById> Customer { get; set; }
    public IList<GetBookingServiceFeedbackId> BookingServiceFeedback { get; set; }
  }
  public class ConfirmAppoinMent 
  {
    public Guid AppointmentId { get; set; }
    public DateTime AppointmentDate { get; set; }
    public DateTime AppointmentTime { get; set; }
    public Guid ConsultantId { get; set; }
    public Guid CustomerId { get; set; }
    public Guid BookingServiceFeedbackId { get; set; }
    public bool IsAccept { get; set; }
    public string FullName { get; set; }
    public string Email { get; set; }
  }

}
