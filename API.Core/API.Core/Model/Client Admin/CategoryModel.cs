using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Context;

namespace API.Core.Model.Client_Admin
{
  public class CategoryModel
  {


    [Key]
    public Guid CategoryId { get; set; }
    public string CategoryName { get; set; }
    public Guid ParentCategoryId { get; set; }
    public string CategoryPic { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public IList<CategoryModel> CategoryList { get; set; }
  }
  public class GetAllCategoryList
  {
    public Guid CategoryId { get; set; }
    public string CategoryName { get; set; }
    public Guid ParentCategoryId { get; set; }
    public string CategoryPic { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
  }
  public class AddCategory : BaseEntity
  {
    public string CategoryName { get; set; }
    public Guid ParentCategoryId { get; set; }

  }
}
