using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Model.Client_Admin
{
   public class BookingServiceFeedbackModel
   {
    [Key]
    public Guid ServiceFeedbackId { get; set; }
    public string BookingServiceCode { get; set; }
    public Guid CustomerId { get; set; }
    public Guid ConsultantId { get; set; }
    public int? ServiceRating { get; set; }
    public string CustomerComment { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsDeleted { get; set; }
    public DateTime CommentDateTime { get; set; }
    public DateTime UpdatedDateTime { get; set; }

  }

  public class AddBookingServiceFeedbackModel
  {
    public string BookingServiceCode { get; set; }
    public Guid CustomerId { get; set; }
    public Guid ConsultantId { get; set; }
    public int? ServiceRating { get; set; }
    public string CustomerComment { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsDeleted { get; set; }
    public DateTime CommentDateTime { get; set; }
    public DateTime UpdatedDateTime { get; set; }
  }
  public class UpdateBookingServiceFeedbackModel
  {
    public Guid ServiceFeedbackId { get; set; }
    public string BookingServiceCode { get; set; }
    public Guid CustomerId { get; set; }
    public Guid ConsultantId { get; set; }
    public int? ServiceRating { get; set; }
    public string CustomerComment { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsDeleted { get; set; }
    public DateTime CommentDateTime { get; set; }
    public DateTime UpdatedDateTime { get; set; }

  }
  public class GetBookingServiceFeedbackId
  {
    public Guid ServiceFeedbackId { get; set; }
    public string BookingServiceCode { get; set; }
    public int? ServiceRating { get; set; }
    public string CustomerComment { get; set; }
    public Guid ConsultantId { get; set; }
    public Guid CustomerId { get; set; }
    public IList<GetConsultantById> Consultant { get; set; }
    public IList<GetCustomerById> Customer { get; set; }
  }

  public class GetAllBookingServiceFeedbackList
  {
    public Guid ServiceFeedbackId { get; set; }
    public string BookingServiceCode { get; set; }
    public int? ServiceRating { get; set; }
    public string CustomerComment { get; set; }
    public Guid ConsultantId { get; set; }
    public Guid CustomerId { get; set; }
    public IList<GetAllConsultantList> Consultant { get; set; }
    //public IList<GetCustomerById> Customer { get; set; }
  }

}
