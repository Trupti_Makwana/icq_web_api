using API.Context.Interface;
using API.Core.Model;
using API.Core.Model.Client_Admin;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace API.Core
{
    public interface IDataContext : IContext
    {
    DbSet<ConsultantDto> Consultant { get; set; }
    DbSet<ConsultantAddressModel> ConsultantAddress { get; set; }
    DbSet<CustomerDto> Customer { get; set; }
    DbSet<CustomerAddressModel> CustomerAddress { get; set; }
    DbSet<CategoryModel> Category { get; set; }
    DbSet<ContactUsModel> ContactUs { get; set; }
    DbSet<CityModel> City { get; set; }
    DbSet<BookingModel> Booking { get; set; }
    DbSet<TableOfTablemodel> TableOfTable { get; set; }
    DbSet<CustomerOTP> CustomersOTP { get; set; }
    DbSet<ConsultantDocumentsModel> ConsultantDocuments { get; set; }
    DbSet<AppoinMentModel> AppoinMent { get; set; }
    DbSet<BookingServiceFeedbackModel> BookingServiceFeedback { get; set; }

  }
}
